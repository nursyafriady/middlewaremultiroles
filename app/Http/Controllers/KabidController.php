<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KabidController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('IsKadis');
        $this->middleware('IsKabid');
        $this->middleware('IsKasi');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('pages.kabid');
    }
}
