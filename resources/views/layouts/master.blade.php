<!DOCTYPE html>
<html lang="en">
<head>  
  @include ('includes.meta')
  @stack('before-style')
  @include ('includes.style')
  @stack('after-style')
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  @include ('includes.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include ('includes.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->   

    <!-- Main content -->
  @yield('content')
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include ('includes.footer')
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@stack('before-scripts')
@include ('includes.scripts')
@stack('after-scripts')
</body>
</html>
