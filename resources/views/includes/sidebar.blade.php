<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src=" {{ asset('backend/dist/img/AdminLTELogo.png') }} " alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"> {{ auth()->user()->roles ?? '' }} ROLES </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src=" {{ asset('backend/dist/img/user2-160x160.jpg') }} " class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block text-uppercase"> {{ auth()->user()->name ?? '' }}  </a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav navbar-nav  nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          @if(Auth::user() && Auth::user()->roles == 'SUPER' || Auth::user() && Auth::user()->roles == 'ADMIN')
          <li class="nav-item nav-sidebar {{ (request()->is('dashboard')) ? 'active' : '' }}">
            <a href="{{ route('home') }}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li> 
          @endif
          @if(Auth::user() && Auth::user()->roles == 'SUPER' || Auth::user() && Auth::user()->roles == 'ADMIN')         
          <li class="nav-item">
            <a href="{{ route('admin') }}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Admin
              </p>
            </a>
          </li>
          @endif
          @if(Auth::user() && Auth::user()->roles == 'SUPER' || Auth::user() && Auth::user()->roles == 'ADMIN')          
          <li class="nav-item">
            <a href="{{ route('kadis') }}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Kadis
              </p>
            </a>
          </li>
          @endif
          @if(Auth::user() && Auth::user()->roles == 'SUPER' || Auth::user() && Auth::user()->roles == 'ADMIN')
          <li class="nav-item">
            <a href="{{ route('kabid') }}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Kabid
              </p>
            </a>
          </li>
          @endif
          @if(Auth::user() && Auth::user()->roles == 'SUPER' || Auth::user() && Auth::user()->roles == 'ADMIN')
          <li class="nav-item">
            <a href="{{ route('kasi') }}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Kasi
              </p>
            </a>
          </li>
          @endif
          @if(Auth::user() && Auth::user()->roles == 'SUPER' || Auth::user() && Auth::user()->roles == 'ADMIN' || Auth::user() && Auth::user()->roles == 'KADIS' || Auth::user() && Auth::user()->roles == 'KABID' || Auth::user() && Auth::user()->roles == 'KASI')
          <li class="nav-item">
            <a href="{{ route('dokumen') }}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dokumen
              </p>
            </a>
          </li>
          @endif
          @if(Auth::user() && Auth::user()->roles == 'SUPER' || Auth::user() && Auth::user()->roles == 'ADMIN')
          <div class="dropdown show">
            <a class="nav-sidebar btn btn-secondary dropdown-toggle 
                      @if(Request::is('kadis')) active @endif
                      @if(Request::is('kabid')) active @endif
                      @if(Request::is('kasi')) active @endif" 
               href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Dropdown link
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <a class="dropdown-item" href="{{ route('kadis') }}">KADIS</a>
              <a class="dropdown-item" href="{{ route('kabid') }}">KABID</a>
              <a class="dropdown-item" href="{{ route('kasi') }}">KASI</a>
            </div>
          </div>
          @endif
          @if(Auth::user() && Auth::user()->roles == 'SUPER')
          <li class="nav-item">
            <a href="{{ route('user') }}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                USER
              </p>
            </a>
          </li>
          @endif
          <li class="nav-item">
            <a href="{{ route('logout') }}" 
               href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();"class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Logout
              </p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>