<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/dashboard', 'HomeController@index')->name('home');
// Route::get('/admin', 'AdminController@index')->name('admin');
// Route::get('/kadis', 'KadisController@index')->name('kadis');
// Route::get('/kabid', 'KabidController@index')->name('kabid');
// Route::get('/kasi', 'KasiController@index')->name('kasi');
// Route::get('/user', 'UserController@index')->name('user');
// Route::get('/dokumen', 'DokumenController@index')->name('dokumen');

Route::prefix('/')
        ->middleware(['auth'])
        ->group(function () {
            Route::get('dashboard', 'HomeController@index')->name('home');
            Route::get('admin', 'AdminController@index')->name('admin');
            Route::get('kadis', 'KadisController@index')->name('kadis');
            Route::get('kabid', 'KabidController@index')->name('kabid');
            Route::get('kasi', 'KasiController@index')->name('kasi');
            Route::get('user', 'UserController@index')->name('user');
            Route::get('dokumen', 'DokumenController@index')->name('dokumen');
        });

Auth::routes();
